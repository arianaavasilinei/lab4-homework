#pragma once
#include<istream>
#include<ostream>
using namespace std;

class Complex
{
public:
	Complex();
	Complex(float real, float imaginar);

	Complex add(Complex);
	Complex subtraction(Complex);
	Complex multiplication(Complex);
	Complex division(Complex);

	Complex operator+(Complex);
	Complex operator-(Complex); 
	Complex operator*(Complex);
	Complex operator/(Complex);

	friend ostream& operator<<(ostream& os, const Complex& c);
	friend istream& operator>>(istream& is, Complex& c);

	friend bool operator==(const Complex&, const Complex&);
	friend bool operator!=(const Complex&, const Complex&);

private:
	float m_real, m_imaginar;
};