#include "DynamicArray.h"

DynamicArray::DynamicArray()
{
	this->capacity = 10;
	this->nrElem = 0;
	this->elem = new Complex[capacity];
}

DynamicArray::~DynamicArray()
{
	delete[] this->elem;
}

DynamicArray::DynamicArray(const DynamicArray& newArr)
{
	this->capacity = newArr.capacity;
	this->nrElem = newArr.nrElem;
	this->elem = new Complex[this->capacity];
	for (int i = 0; i < this->nrElem; i++)
	{
		elem[i] = newArr.elem[i];
	}
}

void DynamicArray::resize()
{
	int newCapacity = this->capacity * 10;
	Complex* aux = new Complex[newCapacity];
	for (int i = 0; i < this->nrElem; i++)
	{
		aux[i] = this->elem[i];
	}
	delete[] this->elem;
	this->elem = aux;
	this->capacity = newCapacity;
}

void DynamicArray::addLast(Complex a)
{
	if (this->nrElem == this->capacity)
	{
		this->resize();
	}
	this->elem[this->nrElem] = a;
	this->nrElem++;
}

void DynamicArray::removeLast()
{
	delete &this->elem[this->nrElem-1];
	this->nrElem--;
}

int DynamicArray::getCapacity()
{
	return this->capacity;
}

int DynamicArray::getnrElem()
{
	return this->nrElem;
}

Complex& DynamicArray::operator[](int index)
{
	return *(this->elem + index);
}

DynamicArray& DynamicArray::operator=(const DynamicArray& arr)
{
	this->capacity = arr.capacity;
	this->nrElem = arr.nrElem;
	Complex* aux = new Complex[capacity];
	for (int i = 0; i < this->nrElem; i++)
	{
		aux[i] = arr.elem[i];
	}
	delete[] this->elem;
	this->elem = aux;
	return *this;
}

ostream& operator<<(ostream& os, const DynamicArray& arr)
{
	for (int i = 0; i < arr.nrElem; i++)
	{
		os << arr.elem[i] << " ";
	}
	os << endl;
	return os;
}
