#include "complex.h"
Complex::Complex():m_real(0),m_imaginar(0)
{
	
}
Complex::Complex(float m_real, float m_imaginar)
{
	this->m_real = m_real;
	this->m_imaginar = m_imaginar;
}
Complex Complex::add(Complex c)
{
	Complex result;

	result.m_real = this->m_real + c.m_real;
	result.m_imaginar = this->m_imaginar + c.m_imaginar;

	return result;
}

Complex Complex::subtraction(Complex c)
{
	Complex result;

	result.m_real = this->m_real - c.m_real;
	result.m_imaginar = this->m_imaginar - c.m_imaginar;

	return result;
}

Complex Complex::multiplication(Complex c)
{
	Complex result;
	
	result.m_real = (this->m_real * c.m_real) - (this->m_imaginar * c.m_imaginar);
	result.m_imaginar = (this->m_real * c.m_imaginar) - (this->m_imaginar * c.m_real);

	return result;
}

Complex Complex::division(Complex c)
{
	Complex result;

	result.m_real = (this->m_real * c.m_real + this->m_imaginar * c.m_imaginar) / (c.m_real * c.m_real + c.m_imaginar * c.m_imaginar);
	result.m_imaginar = (this->m_imaginar * c.m_real - this->m_real * c.m_imaginar) / (c.m_real * c.m_real + c.m_imaginar * c.m_imaginar);

	return result;
}

Complex Complex::operator+(Complex number)
{
	return this->add(number);
}

Complex Complex::operator-(Complex number)
{
	return this->subtraction(number);
}

Complex Complex::operator*(Complex number)
{
	return this->multiplication(number);
}

Complex Complex::operator/(Complex number)
{
	return this->division(number);
}



ostream& operator<<(ostream& os, const Complex& c)
{
	os << c.m_real << "+" <<c.m_imaginar << endl;
	return os;
}

istream& operator>>(istream& is, Complex& c)
{
	is >> c.m_real >> c.m_imaginar;
	return is;
}

bool operator==(const Complex& a, const Complex& b)
{
	if ((a.m_real == b.m_real) && (a.m_imaginar == b.m_imaginar))
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool operator!=(const Complex& a, const Complex& b)
{
	if ((a.m_real == b.m_real) && (a.m_imaginar == b.m_imaginar))
	{
		return false;
	}
	else
	{
		return true;
	}
}
