#pragma once
#include "complex.h"
class DynamicArray
{
public:
	//operator-
	//copy constructor
	//destructor
	//^rule of 3
	//addLast
	//operator+
	//operator[]->elementul din m_arr de pe pozitia i Complex& operator[](int i);
	//operator<<
	DynamicArray();
	~DynamicArray();
	DynamicArray(const DynamicArray&);
	void resize();
	void addLast(Complex);
	void removeLast();
	int getCapacity();
	int getnrElem();
	friend ostream& operator<<(ostream& os, const DynamicArray&);
	Complex& operator[](int index);
	DynamicArray& operator=(const DynamicArray&);
private:
	int nrElem;
	int capacity;
	Complex* elem;
};